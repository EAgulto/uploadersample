const path = require('path');
const fs = require('fs')
var uploadPath = __dirname;

exports.upload = async(req, res) => {
    req.pipe(req.busboy);
    req.busboy.on("field", (key, value) => {
        console.log(key, value);
    })
    req.busboy.on("file", (name, file, filename) => {
        console.log('Uploading');
        var newFilename = Date.now() + "-" + filename.filename;
        const fstream = fs.createWriteStream(path.join(uploadPath, newFilename)); //From the upload stream, an fs.createWriteSteam was used in order to not load the whole file to memory and write it as soon as the backend receives some data chunks.
        file.pipe(fstream);
        fstream.on("error", (err) => {
          console.log(err);
          res.send("error");
        });
        fstream.on("close", () => {
            console.log("uploaded " + filename.filename);
            res.send('ok')
          });
    })
}