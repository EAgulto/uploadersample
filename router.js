const uploadController = require("./uploadController")

const busboy = require('connect-busboy');
const router = require('express').Router();

router.use(busboy({
    highWaterMark: 2*1024*1024,
}));

router.post('/upload', uploadController.upload);
module.exports = router;